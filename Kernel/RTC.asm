 
GLOBAL sysRTCRead
GLOBAL sysRTCSet

section .text

sysRTCRead:
	push rbp
	mov rbp, rsp
	mov dx, 70h
	mov rax, rdi
	OUT dx, al
	IN al, 71h
	mov rsp, rbp
	pop rbp
	ret

sysRTCSet:
	push rbp
	mov rbp, rsp
	mov dx, 70h
	mov rax, rdi
	OUT dx, al
	mov rax, rsi
	mov dx, 71h
	OUT dx, al
	mov rsp, rbp
	pop rbp
	ret	

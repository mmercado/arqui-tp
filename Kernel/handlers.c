#include <handlers.h>
#include <RTC.h>
#include <video.h>
#include <keyboard.h>

int sysCallHandler(uint32_t arg1, uint32_t arg2, uint32_t arg3, uint32_t arg4){
	char time;
	
	switch(arg4){
		
		case SYS_WRITE:
			sysWrite(arg1, (char*)arg2, arg3);
			break;
		
		case SYS_READ:
			return sysRead(arg1, (char*)arg2, arg3);
		
		case SYS_SSTIME:
			kernelSetSSTime(arg1);
			break;
		
		case SYS_CLEAR:
			clearScreen();
			break;
		
		case SYS_RTC_READ:
			time = sysRTCRead(arg1);
			readBCD(time, arg2);
			break;

		case SYS_RTC_SET:
			sysRTCSet(arg1, arg2);
			break;

		default:
			break;
	}
	return 0;
}

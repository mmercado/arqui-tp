
#ifndef ATTRIBUTES_H_
#define ATTRIBUTES_H_


/* Flags for segments access privilege level */
#define ACS_PRESENT     0x80            /* Segment presence in memory */
#define ACS_CSEG        0x18            /* Code Segment */
#define ACS_DSEG        0x10            /* Data Segment */
#define ACS_READ        0x02            /* Read Segment */
#define ACS_WRITE       0x02            /* Write Segment */
#define ACS_IDT         ACS_DSEG
#define ACS_INT_386 	0x0E		/* Interrupt GATE 32 bits */
#define ACS_INT         ( ACS_PRESENT | ACS_INT_386 )


#define ACS_CODE        (ACS_PRESENT | ACS_CSEG | ACS_READ)
#define ACS_DATA        (ACS_PRESENT | ACS_DSEG | ACS_WRITE)
#define ACS_STACK       (ACS_PRESENT | ACS_DSEG | ACS_WRITE)


#endif



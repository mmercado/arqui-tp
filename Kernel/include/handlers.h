
#ifndef HANDLERS_H_
#define HANDLERS_H_

#include "types.h"
#include "RTC.h"
#include "video.h"
#include "klib.h"

#define HOURS 0x04
#define MINUTES 0x02
#define SECONDS 0x00

#define SYS_READ 1
#define SYS_WRITE 2
#define SYS_RTC_READ 3
#define SYS_RTC_SET 4
#define SYS_SSTIME 5
#define SYS_CLEAR 6


int sysCallHandler(uint32_t arg1, uint32_t arg2, uint32_t arg3, uint32_t arg4);

#endif /* HANDLERS_H_ */

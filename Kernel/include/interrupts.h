
#ifndef INTERRUPS_H_
#define INTERRUPS_H_

#include "types.h"

void _cli(void);
void _sti(void);


void picMasterMask(uint8_t mask);
void picSlaveMask(uint8_t mask);

void _irq00Handler(void);
void _irq01Handler(void);

void _int80Handler(uint32_t arg1, uint32_t arg2, uint32_t arg3, uint32_t arg4);

#endif /* INTERRUPS_H_ */

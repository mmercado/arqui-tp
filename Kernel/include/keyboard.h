#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#define STDIN 1

#define NOT_PRINTABLE (unsigned char) 0
#define SHIFTL_MAKE (unsigned char)0x2A
#define SHIFTL_BREAK (unsigned char)0xAA
#define SHIFTR_MAKE (unsigned char)0x36
#define SHIFTR_BREAK (unsigned char)0xB6
#define CTRL_MAKE (unsigned char)0x1D
#define CTRL_BREAK (unsigned char)0x9D

#define BUFF_SIZE 128
#define SCREEN_SIZE 80*25*2

int sysRead(int fd, char* buffer, int length);

char * keyboard(unsigned char code);


/*
 * Sets the time it takes for the screen saver to activate.
*/
void kernelSetSSTime(int times);

void screenSaver();


	static char scanCodes[][3] = { { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*000*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*001 ESCAPE*/
    { '1', '!', NOT_PRINTABLE }, /*002*/
    { '2', '\"', NOT_PRINTABLE }, /*003*/
    { '3', '#', NOT_PRINTABLE }, /*004*/
    { '4', '$', NOT_PRINTABLE }, /*005*/
    { '5', '%', NOT_PRINTABLE }, /*006*/
    { '6', '&', NOT_PRINTABLE }, /*007*/
    { '7', '/', NOT_PRINTABLE }, /*008*/
    { '8', '(', NOT_PRINTABLE }, /*009*/
    { '9', ')', NOT_PRINTABLE }, /*010*/
    { '0', '=', NOT_PRINTABLE }, /*011*/
    { '\'', '?', NOT_PRINTABLE }, /*012*/
    { '\n', '\n', NOT_PRINTABLE }, /*013*/
    { '\b', '\b', NOT_PRINTABLE }, /*014 BACKSPACE*/
    { '\t', '\t', NOT_PRINTABLE }, /*015 TAB*/
    { 'q', 'Q', NOT_PRINTABLE }, /*016*/
    { 'w', 'W', NOT_PRINTABLE }, /*017*/
    { 'e', 'E', NOT_PRINTABLE }, /*018*/
    { 'r', 'R', NOT_PRINTABLE }, /*019*/
    { 't', 'T', NOT_PRINTABLE }, /*020*/
    { 'y', 'Y', NOT_PRINTABLE }, /*021*/
    { 'u', 'U', NOT_PRINTABLE }, /*022*/
    { 'i', 'I', NOT_PRINTABLE }, /*023*/
    { 'o', 'O', NOT_PRINTABLE }, /*024*/
    { 'p', 'P', NOT_PRINTABLE }, /*025*/
    { '\'', '\"', NOT_PRINTABLE }, /*026*/
    { '+', '*', NOT_PRINTABLE }, /*027*/
    { '\n', '\n', '\n' }, /*028*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*029 CTRL IZQ*/
    { 'a', 'A', NOT_PRINTABLE }, /*030*/
    { 's', 'S', NOT_PRINTABLE }, /*031*/
    { 'd', 'D', NOT_PRINTABLE }, /*032*/
    { 'f', 'F', NOT_PRINTABLE }, /*033*/
    { 'g', 'G', NOT_PRINTABLE }, /*034*/
    { 'h', 'H', NOT_PRINTABLE }, /*035*/
    { 'j', 'J', NOT_PRINTABLE }, /*036*/
    { 'k', 'K', NOT_PRINTABLE }, /*037*/
    { 'l', 'L', NOT_PRINTABLE }, /*038*/
    { ':', ';', NOT_PRINTABLE }, /*039*/
    { '{', '[', NOT_PRINTABLE }, /*040*/
    { '}', ']', NOT_PRINTABLE }, /*041*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*042 SHIFT IZQ*/
    { '<', '>', NOT_PRINTABLE }, /*043*/
    { 'z', 'Z', NOT_PRINTABLE }, /*044*/
    { 'x', 'X', NOT_PRINTABLE }, /*045*/
    { 'c', 'C', NOT_PRINTABLE }, /*046*/
    { 'v', 'V', NOT_PRINTABLE }, /*047*/
    { 'b', 'B', NOT_PRINTABLE }, /*048*/
    { 'n', 'N', NOT_PRINTABLE }, /*049*/
    { 'm', 'M', NOT_PRINTABLE }, /*050*/
    { ',', ';', NOT_PRINTABLE }, /*051*/
    { '.', ':', NOT_PRINTABLE }, /*052*/
    { '-', '_', '/' }, /*053*/ /* / */
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*054 SHIFT DER*/
    { '*', '*', NOT_PRINTABLE}, /*055 KEY **/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*056 ALT IZQ*/ /* ALT DER*/
    { ' ', ' ', NOT_PRINTABLE }, /*057 SPACE*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*058 CAPSLOCK*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*059 F1*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*060 F2*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*061 F3*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*062 F4*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*063 F5*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*064 F6*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*065 F7*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*066 F8*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*067 F9*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*068 F10*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*069 NUM LOCK*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE }, /*070 SCROLL LOCK*/
    { NOT_PRINTABLE, '7', NOT_PRINTABLE }, /*071 KEY 7*/ /* home */
    { NOT_PRINTABLE, '8', NOT_PRINTABLE }, /*072 KEY 8*/ /* upper arrow */
    { NOT_PRINTABLE, '9', NOT_PRINTABLE }, /*073 KEY 9*/ /* repag */
    { '-', '-' , NOT_PRINTABLE}, /*074 KEY -*/
    { NOT_PRINTABLE, '4', NOT_PRINTABLE }, /*075 KEY 4*/ /* left arrow */
    { NOT_PRINTABLE, '5', NOT_PRINTABLE }, /*076 KEY 5*/
    { NOT_PRINTABLE, '6', NOT_PRINTABLE }, /*077 KEY 6*/ /* right arrow */
    { '+', '+', NOT_PRINTABLE }, /*078 KEY +*/
    { NOT_PRINTABLE, '1', NOT_PRINTABLE }, /*079 KEY 1*/ /* end */
    { NOT_PRINTABLE, '2', NOT_PRINTABLE }, /*080 KEY 2*/ /* lower arrow */
    { NOT_PRINTABLE, '3', NOT_PRINTABLE }, /*081 KEY 3*/ /* avpag */
    { NOT_PRINTABLE, '0', NOT_PRINTABLE }, /*082 KEY 0*/ /* insert */
    { '.', '.', NOT_PRINTABLE }, /*083 KEY .*/ /* supr */
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*084 SYS REQ (AT)*/
    { '+', '*', NOT_PRINTABLE }, /*085*/
    { '<', '>', NOT_PRINTABLE }, /*086*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*087 F11*/
    { NOT_PRINTABLE, NOT_PRINTABLE, NOT_PRINTABLE },/*088 F12*/
    { '+', '*', NOT_PRINTABLE }, /*089*/
    { '+', '*', NOT_PRINTABLE } /*090*/
};

#endif /* KEYBOARD_H_ */
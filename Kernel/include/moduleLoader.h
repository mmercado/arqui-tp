#ifndef MODULELOADER_H_
#define MODULELOADER_H_

void loadModules(void * payloadStart, void ** moduleTargetAddress);

#endif
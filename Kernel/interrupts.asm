GLOBAL _cli
GLOBAL _sti
GLOBAL picMasterMask
GLOBAL picSlaveMask

GLOBAL _int80Handler
GLOBAL _irq00Handler
GLOBAL _irq01Handler

EXTERN keyboard
EXTERN sysCallHandler
EXTERN screenSaver

%macro pushaq 0
    push rax      ;save current rax
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
%endmacro

%macro popaq 0
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rsi
    pop rdi
    pop rbp
    pop rdx
    pop rcx
    pop rbx
    pop rax
%endmacro

SECTION .text

_cli:
	cli
	ret

_sti:
	sti
	ret

picMasterMask:
	push rbp
    mov rbp, rsp
    pushaq
    mov rax, rdi
    out	21h,al
    popaq
    mov rsp, rbp
    pop rbp
    retn

picSlaveMask:
	push rbp
    mov rbp, rsp
    pushaq
    mov rax, rdi
    out	0A1h,al
    popaq
    mov rsp, rbp
    pop rbp
    retn


;8254 Timer (Timer Tick)
_irq00Handler:
	push rbp
	mov rbp, rsp
	pushaq
	call screenSaver
	mov al,20h			
	out	20h,al
	popaq
	mov rsp, rbp
	pop rbp
	iretq
	
	
;Keyboard
_irq01Handler:
	push rbp
	mov rbp, rsp
	pushaq
	
    xor rax, rax
	in al, 60h
	mov rdi, rax
	call keyboard
	mov rdi, rax
	mov al,20h			
	out	20h,al
	
	popaq
	mov rsp, rbp
	pop rbp
	iretq


_int80Handler:
	push rbp
	mov rbp, rsp
	
	push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15

	call sysCallHandler
	
	pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rsi
    pop rdi
    pop rbp
    pop rdx
    pop rcx
    pop rbx

	mov rsp, rbp
	pop rbp
	iretq

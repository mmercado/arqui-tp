#include <stdint.h>
#include <klib.h>
#include <moduleLoader.h>

static void loadModule(uint8_t ** module, void * targetModuleAddress);
static uint32_t readUint32(uint8_t ** address);

void loadModules(void * payloadStart, void ** targetModuleAddress)
{
	int i;
	uint8_t * currentModule = (uint8_t*)payloadStart;
	uint32_t moduleCount = readUint32(&currentModule);

	for (i = 0; i < moduleCount; i++){
		//currentModule: Points to the module
		//targetModuleAdress: The adress where the module is beign moved
		loadModule(&currentModule, targetModuleAddress[i]);
	}	
}

static void loadModule(uint8_t ** module, void * targetModuleAddress)
{	
	//Reads the size and moves the module pointer 32 bits down, module now points the module itself
	uint32_t moduleSize = readUint32(module);

	//Copy the module to the target adress
	memcpy(targetModuleAddress, *module, moduleSize);
	*module += moduleSize;
}

static uint32_t readUint32(uint8_t ** address)
{
	//Get the ammount of modules
	uint32_t result = *(uint32_t*)(*address);
	//Make adress point to Size of module
	*address += sizeof(uint32_t);
	return result;
}

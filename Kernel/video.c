#include <video.h>

static uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base);

static char buffer[128] = { 0 };
static uint8_t * const video = (uint8_t*)DIR_VIDEO;
static uint8_t * currentVideo = (uint8_t*)DIR_VIDEO;
static const uint32_t width = 80;
static const uint32_t height = 25 ;
static const uint32_t screenSize = 80*25*2;

void kPrintColorString(const char * string, char attr)
{
	int i;
	for (i = 0; string[i] != 0; i++)
		kPrintChar(string[i], attr);
}

void kPrint(const char* string, int length){
	int i;

	for (i = 0; i<length; i++)
		kPrintChar(string[i], 0x02);
}

int sysWrite(int fd,const char* buffer, int length){
	if( fd == STDOUT){
		kPrint(buffer, length);
		return TRUE;

	}

	return FALSE;
	
}

void kPrintChar(char c, char attr)
{
	int i = 0;
	switch(c){

		case'\n':
			kNewline(); 
			break;
		
		case '\t':
			for (i=0; i<TAB_SPACES; i++){
				*currentVideo = '\t';
				*(currentVideo+1) = 0x00;
				currentVideo += 2;
			}
			break;

		case '\b':

				currentVideo -= 2;
				
				if ( (((currentVideo-video)%160)>3) && *currentVideo!=0 ) {
					if( *currentVideo!= '\t'){
						*currentVideo = 0;
						*(currentVideo+1) = 0;
					}
					else{
						while(*currentVideo == '\t'){
							*currentVideo = 0;
							*(currentVideo+1) = 0;
							currentVideo -= 2;
						}
						currentVideo += 2;
					}	
				}
				else{	
					
					currentVideo+=2;	
				}

			break;

		default:
			*currentVideo = c;
			*(currentVideo + 1) = attr;
			currentVideo+=2;
			break;
	}
	if ((currentVideo-video)>screenSize){
		scroll();
		
	}  
	
}

void scroll(){
	uint8_t * x;
	uint8_t * y;
	for (x =(uint8_t*)DIR_VIDEO, y =(uint8_t*)DIR_VIDEO + 160; y < currentVideo; y+=2, x+=2){
		*x = *y;
		*(x+1) = *(y+1);
	}
	currentVideo-=160;
	int i;
	for(i = 0;i < 160 ; i+=2){
		*(currentVideo+i) = 0;
		*(currentVideo+i+1) = 0;

	}
}

void kNewline()
{
	do
	{
		kPrintChar('\0', 0x0F);
	}
	while((uint64_t)(currentVideo - video) % (width * 2) != 0);
}

void kPrintDec(uint64_t value)
{
	kPrintBase(value, 10);
}

void kPrintHex(uint64_t value)
{
	kPrintBase(value, 16);
}

void kPrintBin(uint64_t value)
{
	kPrintBase(value, 2);
}

void kPrintBase(uint64_t value, uint32_t base)
{
    uintToBase(value, buffer, base);
    kPrintColorString(buffer, 0x0F);
}

void clearScreen(){
	int i;
	for(i=0; i<screenSize; i+=2){
		*(video+i) = 0;
	}
	currentVideo=video;
}

static uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base)
{
	char *p = buffer;
	char *p1, *p2;
	uint32_t digits = 0;

	//Calculate characters for each digit
	do
	{
		uint32_t remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	}
	while (value /= base);

	// Terminate string in buffer.
	*p = 0;

	//Reverse string in buffer.
	p1 = buffer;
	p2 = p - 1;
	while (p1 < p2)
	{
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}

	return digits;
}

uint8_t* backupScreen(char* b){
	int i;
	for(i=0; i<screenSize; i++){
		b[i]=*(video+i);
	}
	return currentVideo;
}

void resetScreen(char* b, uint8_t* dir){
	int i;
	currentVideo = dir;
	for(i=0; i<screenSize; i++){
		*(video+i) = b[i];
	}
}

void applyScreenSaver(){
	currentVideo = video;
	kNewline();
	kNewline();
	kNewline();
	kPrintColorString("\n\n\n\t\t\tDA BEST SCREEN SAVER YOU'VE EVER SEEN\n\n\t\t\t\t    (Now on color!)", 0x02);
}

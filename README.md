# arqui-tp #

A simple kernel implementation. Offers interruptions functions, keyboard and video drivers, and some user programs like getting/setting RTC time, setting a screen saver, etc.

More detailed information about the project implementation can be found in 'Informe TPE.docx'.

### Contributors ###
* gmogni
* mmercado
* mmorenolafon
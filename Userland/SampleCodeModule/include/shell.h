#ifndef SHELL_H_
#define SHELL_H_

#define SHELL_BUFF_SIZE 128
#define TOTAL_COMMANDS 5

typedef enum { HELP=0, SETTIME, VIEWTIME, SETSSTIME, CLEAR }command;


/*
 * Reads from STDIN and prints on STDOUT until a '\n' is found.
 * Saves the input in Shell Buffer. 
 * 
*/
int scanf();

/*
 * Searches for commands in the Shell Buffer.
*/
void shell();

int splitArgs(char* buf, int from, char** args, int countParam);

void viewTime();

/*
 * The execute functions recieve an index indicating from where to start reading the arguments
 * in the shell buffer.
 * Commands that do not need arguments will recieve index 0.
*/
void executeHelp(int index);

void executeSetTime(int index);

void executeViewTime(int index);

void executeSetSSTime(int index);

#endif /* SHELL_H_ */
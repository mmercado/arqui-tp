#ifndef USER_ASM_LIB_
#define USER_ASM_LIB_

int read(int fd, char* buf, int length);

void write(int fd, char* buf, int length);

/*
 * timeRegister: HOURS, MINUTES, SECONDS
*/
void readTime(char timeRegister, char * time);

void setTime(char timeRegister, char time);

/*
 * Sets the time it takes the Screen Saver to activate	
*/
void setSSTime(int times);

void clear();

#endif

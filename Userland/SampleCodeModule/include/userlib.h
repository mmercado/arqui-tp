
#ifndef USER_LIB_
#define USER_LIB_
#include <userASMlib.h>
#include <stdint.h>

#define HOURS 0x04
#define MINUTES 0x02
#define SECONDS 0x00

#define TRUE 1
#define FALSE 0

void printChar(char c);

void printf (const char *format, ...);

char getChar();

void itoa(int num, char arr[]);

int atoi(char * arr);

int strlen(char * str);

int strcmp(char* str1, char* str2);

void printBase(uint64_t value, uint32_t base);

void printString(char * arg);

void printDec(uint64_t value);

void printHex(uint64_t value);

uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base);

void setLocalTime();

#endif

#include <shell.h>
#include <userlib.h>

static char shellBuffer[SHELL_BUFF_SIZE];


char* cmd[]={"help", "settime", "viewtime", "setsstime","clear"};

void shell(){
	int i;
	int length;

	for (i=0; i<TOTAL_COMMANDS; i++){

		length = strcmp(cmd[i],shellBuffer);

		if(length > 0){

			switch(i){
				
				case HELP:
					executeHelp(0);
					return;
				case SETTIME:
					executeSetTime(length+1);
					return;
				case VIEWTIME:
					executeViewTime(0);
					return;
				case SETSSTIME:
					executeSetSSTime(length+1);
					return;
					
				case CLEAR:
					if(shellBuffer[length]==0){
						clear();
						return;
					}
					break;	
				default:
					printf("Invalid command\n");
					return;
			}
		}

	}
	
	printf("Invalid command\n");
	return;
}


void executeHelp(int index){
	if (shellBuffer[4]!=0){
		printf("Invalid command\n");
		return;
	}
	
	printf("\nSupported Commands:\n");
	
	for (int i = 1; i < TOTAL_COMMANDS; ++i){
		if(i != HELP){
		printf("-%s\n",cmd[i]);
	
		}
	}

	printChar('\n');
	
}

void executeSetTime(int index){
	char c1[3];
	char c2[3];
	char c3[3];
	char * args[]={c1, c2, c3};
	int countArgs;
	countArgs = splitArgs(shellBuffer, index, args, 3);

	if (countArgs!=3){
		printf("Invalid command\n");
		return;
	}

	int count1;
	int count2;
	int count3;
	count1 = atoi(args[0]);

	if(count1>23 || count1<0){
		printf("Invalid command\n");
		return;
	}

	count2 = atoi(args[1]);

	if (count2>60 || count2<0){
		printf("Invalid command\n");
		return;
	}

	count3 = atoi(args[2]);

	if (count3>60 || count3<0){
		printf("Invalid command\n");
		return;
	}
	
	count1+=(count1/10)*6;
	count2+=(count2/10)*6;
	count3+=(count3/10)*6;
	
	setTime(HOURS, count1);
	setTime(MINUTES, count2);
	setTime(SECONDS, count3);
	
}

void executeViewTime(int index){
	if(shellBuffer[8]!=0){
		printf("Invalid command\n");
		return;
	}
    viewTime();
}

void viewTime(){
    char hours[3];
    char minutes[3];
    char seconds[3];
    readTime(HOURS, hours);
    readTime(MINUTES, minutes);
    readTime(SECONDS, seconds);
	printf("    %s:%s:%s\n",hours, minutes, seconds);
   
}

void executeSetSSTime(int index){
	int count;
	count = atoi(shellBuffer+index);
	if (count<0){
		printf("Invalid command\n");
		return;
	}
	setSSTime(count);
}

/*
* Recieves: 
* 	buf: Buffer from where arguments are read. 
* 	args: Destination array, where the arguments are going to be copied.
* 	from: Indicates from where to start reading the buffer
* Returns: Total arguments read
*/

int splitArgs(char* buf, int from, char** args, int countParam){
	int index = from;
	int currentIndex = 0;
	int argsIndex = 0;
	while(buf[index]!=0){
		if(buf[index]==':'){
			args[argsIndex][currentIndex]=0;
			currentIndex=0;
			argsIndex++;
		}else{
			args[argsIndex][currentIndex]=buf[index];
			currentIndex++;
	

		}
		if(argsIndex>=countParam){
			return -1;
		} 
		index++;
	}
	args[argsIndex][currentIndex]=0;
	argsIndex++;
	if (argsIndex!= countParam){
		return -1;
	}
	return argsIndex;
}


int scanf(){
	
	int i;
	for(i=0; i<SHELL_BUFF_SIZE; i++){
		shellBuffer[i] = 0;
	}
	unsigned char index = 0;
	while((shellBuffer[index % SHELL_BUFF_SIZE] = getChar())!='\n'){
		printChar(shellBuffer[index % SHELL_BUFF_SIZE]);
		if(shellBuffer[index % SHELL_BUFF_SIZE] != '\b'){
			index++;
		}
		else{
			if (index>0){
				index--;
			}
		}			
	}
	printChar('\n');
	shellBuffer[index % SHELL_BUFF_SIZE]=0;
	return index;
}






GLOBAL read
GLOBAL write
GLOBAL readTime
GLOBAL setTime
GLOBAL setSSTime
GLOBAL clear

setSSTime:
	push rbp
	mov rbp, rsp
	mov rcx, 5
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

clear:
	push rbp
	mov rbp, rsp
	mov rcx, 6
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

read:
	push rbp
	mov rbp, rsp
	mov rcx, 1
loop:
	int 0x80
	cmp rdx, 0
	jz end
	sub rdx, rax
	add rsi, rax
	jmp loop
end:
	mov rsp, rbp
	pop rbp
	ret

write:
	push rbp
	mov rbp, rsp
	mov rcx, 2
	int 0x80
	mov rsp, rbp
	pop rbp
	ret

readTime:
	push rbp
	mov rbp, rsp
	mov rdx, 2
	mov rcx, 3
	int 0x80
	mov rsp, rbp
	pop rbp
	ret	


;setTime:
;rdi: timeRegister
;rsi: time
;rdx: function Code

setTime:
	push rbp
	mov rbp, rsp

	mov rdx, 3
	mov rcx, 4
	int 0x80
	
	mov rsp, rbp
	pop rbp
	ret

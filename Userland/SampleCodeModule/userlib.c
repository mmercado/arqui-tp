#include <userlib.h>
#include <stdarg.h>
#define STDIN 1
#define STDOUT 1

static char printBuffer[20]={0};


int strcmp(char* str1, char* str2){
	int i;
	for(i= 0; str1[i]!= 0; i++){
		if(str1[i]!=str2[i]){
			return 0;
		}
	}
	return i;
}

void printChar(char c){
	write(STDIN, &c, 1);
}

char getChar(){
	static char c;
	read(STDOUT, &c, 1);
	return c;
}

void itoa(int num, char arr[]){
	int digits=0, aux = num;
		
	do{	
		digits++;
		aux/=10;
	} while(aux!=0);
	
	aux = digits;
	while(aux!=0){
		arr[aux-1] = num%10 + '0';
		num/= 10;
		aux--;	
	}
	arr[digits] = 0;
	return;
}

int atoi(char * arr){
	int i=0, num = 0;
	if ((*arr==0 ) || (*arr =='-')){
		return -1;
	}
	while(arr[i] != 0){
		if(arr[i]>'9'|| arr[i]<'0'){
			return -1;
		}
		num*=10;
		num+= arr[i]-'0';
		i++;
	}

	return num;
}

int strlen(char * str){
	int i = 0;
	while(str[i] != 0){
		i++;
	}
	return i;
}

void printString(char * arg){
	int i = 0;
	while(arg[i]!=0){
		printChar(arg[i++]);
	} 
}
void printf (const char *format, ...){
	
	char c;
	int i =0;
	va_list ap;
	va_start(ap, format);
	while ((c = format[i]) != 0){
	   	if (c != '%')
	        printChar(c);
	    else{
	       	i++;
	       	c = format[i];
	       	switch(c){
	       		case 'd':
	       			printDec(va_arg(ap, int));
	       			break;
	       		case 'x':
	       			printHex(va_arg(ap, uint64_t));
	       			break;
	       		case 's':
        			printString(va_arg(ap, char*));
   					break;
   				case 'c':
   					printChar(va_arg(ap,char));
   				default:
   					break;		
	        	}
		}
		i++;			
	}
	va_end(ap);
}

void printDec(uint64_t value)
{
	printBase(value, 10);
	
}

void printHex(uint64_t value)
{
	printBase(value, 16);
}

void printBase(uint64_t value, uint32_t base)
{
	int x;
	for(x=0;x<20;x++){printBuffer[x]=0;} 
    uintToBase(value, printBuffer, base);
    printString(printBuffer);

}

uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base)
{
	char *p = buffer;
	char *p1, *p2;
	uint32_t digits = 0;

	//Calculate characters for each digit
	do
	{
		uint32_t remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	}
	while (value /= base);

	// Terminate string in buffer.
	*p = 0;

	//Reverse string in buffer.
	p1 = buffer;
	p2 = p - 1;
	while (p1 < p2)
	{
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}

	return digits;
}

